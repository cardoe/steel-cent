extern crate steel_cent;

use steel_cent::{Money, formatting};
use std::io::{self, BufRead};
use std::env;

// cargo run --example convert_format us uk
fn main() {
    let mut args = env::args();
    if args.len() != 3 {
        panic!("Usage: convert_format input_style output_style\n  where each style is one of us, uk, france, generic");
    }
    let input_style = style(&(args.nth(1).expect("Failed reading first arg")));
    let output_style = style(&(args.next().expect("Failed reading second arg")));
    let parser = input_style.parser();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let s = line.expect("Failed reading input");
        let m: Money = parser.parse(&s).expect("Failed parsing input as input-style currency string");
        println!("{}", output_style.display_for(&m));
    }
}

fn style(name: &str) -> &'static formatting::FormatSpec {
    match name {
        "us" => formatting::us_style(),
        "uk" => formatting::uk_style(),
        "france" => formatting::france_style(),
        "generic" => formatting::generic_style(),
        _ => panic!("Unknown style: please use one of us, uk, france, generic")
    }
}
