// Copyright 2016 John D. Hume
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use std::ops::{Add, Sub, Mul, Div, Rem, Neg};
use std::cmp::{Ordering, PartialOrd};
use std::i64;
use std::fmt;
use currency::Currency;
use formatting::{self, FormattableMoney, ParseableMoney};

/// A signed amount of money in a certain currency, with the currency's standard number of decimal
/// places.
///
/// Note that the arithmetic `ops` implementations all delegate to the `checked_` methods, panicking
/// on `None`, even in a `release` build. (This is in contrast to primitive ops, which will overflow
/// in a `release` build.)
///
/// A `Money` is 128 bits in size.
///
/// ```
/// # use steel_cent::Money;
/// assert_eq!(16, std::mem::size_of::<Money>());
/// ```
///
/// # Examples
///
/// ```
/// # use steel_cent::Money;
/// # use steel_cent::currency::USD;
/// #
/// let price = Money::of_major_minor(USD, 19, 95);
/// let shipping_and_handling = Money::of_major(USD, 10);
/// let convenience_charge = Money::of_major(USD, 6);
/// let discount: f64 = 1.0 - 0.2; // 20% off
/// let discounted_price = price * discount;
/// let fees = shipping_and_handling + convenience_charge;
/// let total = discounted_price + fees;
/// println!("price: {:?}, discounted_price: {:?}", price, discounted_price);
/// assert_eq!(Money::of_minor(USD, 1596), discounted_price);
/// assert_eq!(Money::of_minor(USD, 3196), total);
/// assert_eq!((price * discount) + shipping_and_handling + convenience_charge, total);
/// ```
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct Money {
    pub currency: Currency,
    amount_minor: i64,
}

impl Money {
    /// Creates a Money from its "minor" unit (e.g. US cents to USD).
    pub fn of_minor(currency: Currency, amount_minor: i64) -> Self {
        Money {
            currency: currency,
            amount_minor: amount_minor,
        }
    }

    /// Creates a Money from its "major" unit (e.g. US dollars to USD).
    pub fn of_major(currency: Currency, amount_major: i64) -> Self {
        Self::of_minor(currency, currency.major_to_minor(amount_major))
    }

    pub fn of_major_minor(currency: Currency, amount_major: i64, amount_minor: i64) -> Self {
        Self::of_minor(currency,
                       currency.major_to_minor(amount_major) + amount_minor)
    }

    pub fn zero(currency: Currency) -> Self {
        Self::of_minor(currency, 0)
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(Money::of_major_minor(USD, -92_233_720_368_547_758, -08), Money::min(USD));
    /// ```
    pub fn min(currency: Currency) -> Self {
        Self::of_minor(currency, i64::MIN)
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(Money::of_major_minor(USD, 92_233_720_368_547_758, 07), Money::max(USD));
    /// ```
    pub fn max(currency: Currency) -> Self {
        Self::of_minor(currency, i64::MAX)
    }

    /// Returns the major unit part of the amount.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(2, Money::of_minor(USD, 2_99).major_part());
    /// ```
    pub fn major_part(&self) -> i64 {
        self.currency.major_part(self.amount_minor)
    }

    /// Returns the minor unit part of the amount.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::USD;
    /// assert_eq!(99, Money::of_minor(USD, 2_99).minor_part());
    /// ```
    pub fn minor_part(&self) -> i64 {
        self.currency.minor_part(self.amount_minor)
    }

    /// Returns the total amount in the currency's minor unit.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::USD;
    /// let two_dollars = Money::of_major(USD, 2);
    /// assert_eq!(200, two_dollars.minor_amount());
    /// ```
    pub fn minor_amount(&self) -> i64 {
        self.amount_minor
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::{GBP, JPY, USD};
    /// let five_usd = Money::of_major(USD, 5);
    /// let three_pound_seventy_five = Money::of_major_minor(GBP, 3, 75);
    /// let gbp_per_usd = 0.75;
    /// let five_eleven_yen = Money::of_major(JPY, 511);
    /// let jpy_per_usd = 102.15;
    /// assert_eq!(three_pound_seventy_five, five_usd.convert_to(GBP, gbp_per_usd));
    /// assert_eq!(five_usd, three_pound_seventy_five.convert_to(USD, gbp_per_usd.recip()));
    /// assert_eq!(510.75, 5.0 * jpy_per_usd); // but JPY has zero decimal places.
    /// assert_eq!(five_eleven_yen, five_usd.convert_to(JPY, jpy_per_usd)); // rounded.
    /// assert_eq!(five_usd, five_eleven_yen.convert_to(USD, jpy_per_usd.recip())); // rounded.
    /// ```
    pub fn convert_to(&self, currency: Currency, conversion_multiplier: f64) -> Self {
        let dec_adjust = 10f64
            .powi(currency.decimal_places() as i32 - self.currency.decimal_places() as i32);
        let amount = (self.amount_minor as f64 * conversion_multiplier * dec_adjust).round() as i64;
        Self::of_minor(currency, amount)
    }

    /// Returns absolute value, except for the minimum value, which cannot be negated.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::USD;
    /// let c = Money::of_major(USD, 100);
    /// assert_eq!(Some(c), c.checked_abs());
    /// assert_eq!(Some(c), (-c).checked_abs());
    /// assert_eq!(None, Money::min(USD).checked_abs());
    /// ```
    pub fn checked_abs(&self) -> Option<Self> {
        if self.amount_minor == i64::MIN {
            None
        } else if self.amount_minor.is_negative() {
            Some(Self::of_minor(self.currency, -self.amount_minor))
        } else {
            Some(*self)
        }
    }

    /// Returns absolute value, except for the minimum value, which cannot be negated.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::USD;
    /// let c = Money::of_major(USD, 100);
    /// assert_eq!(c, c.abs());
    /// assert_eq!(c, (-c).abs());
    /// ```
    ///
    /// # Panics
    ///
    /// Panics for the minimum value.
    pub fn abs(&self) -> Self {
        self.checked_abs().expect("Money abs would overflow")
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(Money::of_minor(USD, 4_01)),
    ///            Money::of_major(USD, 4).checked_add(Money::of_minor(USD, 1)));
    /// assert_eq!(None, Money::max(USD).checked_add(Money::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn checked_add(self, other: Self) -> Option<Self> {
        assert_eq!(self.currency, other.currency);
        self.amount_minor.checked_add(other.amount_minor)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(Money::of_minor(USD, 3_99)),
    ///            Money::of_major(USD, 4).checked_sub(Money::of_minor(USD, 1)));
    /// assert_eq!(None, Money::min(USD).checked_sub(Money::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn checked_sub(self, other: Self) -> Option<Self> {
        assert_eq!(self.currency, other.currency);
        self.amount_minor.checked_sub(other.amount_minor)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(Money::of_major(USD, 8)), Money::of_major(USD, 4).checked_mul(2));
    /// assert_eq!(None, Money::max(USD).checked_mul(2));
    /// assert_eq!(None, Money::min(USD).checked_mul(-1));
    /// ```
    pub fn checked_mul(self, n: i64) -> Option<Self> {
        self.amount_minor.checked_mul(n)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// Checked multiplication by a float with rounding.
    /// Note that a float has less-than-integer precision for very large and very small
    /// amounts of money, which can result in surprising rounding errors.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(Money::of_minor(USD, 8_40)), Money::of_major(USD, 4).checked_mul_f(2.1));
    /// assert_eq!(None, Money::max(USD).checked_mul_f(1.01));
    /// assert_eq!(None, Money::min(USD).checked_mul_f(-1.0));
    /// ```
    pub fn checked_mul_f(self, n: f64) -> Option<Self> {
        let candidate_amount_minor = self.amount_minor as f64 * n;
        let min = ::std::i64::MIN as f64;
        let max = ::std::i64::MAX as f64;
        if (min < candidate_amount_minor) && (candidate_amount_minor < max) {
            Some(Self::of_minor(self.currency, candidate_amount_minor.round() as i64))
        } else {
            None
        }
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(Money::of_major(USD, 2)), Money::of_minor(USD, 4_01).checked_div(2));
    /// assert_eq!(None, Money::of_major(USD, 1).checked_div(0));
    /// assert_eq!(None, Money::min(USD).checked_div(-1));
    /// ```
    pub fn checked_div(self, n: i64) -> Option<Self> {
        self.amount_minor.checked_div(n)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(Money::of_minor(USD, 1)), Money::of_minor(USD, 4_01).checked_rem(2));
    /// assert_eq!(None, Money::of_major(USD, 1).checked_rem(0));
    /// assert_eq!(None, Money::min(USD).checked_rem(-1));
    /// ```
    pub fn checked_rem(self, n: i64) -> Option<Self> {
        self.amount_minor.checked_rem(n)
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// Negates the value, except for the minimum value, which cannot be negated.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Some(Money::of_major(USD, -1)), Money::of_major(USD, 1).checked_neg());
    /// assert_eq!(None, Money::min(USD).checked_neg());
    /// ```
    pub fn checked_neg(self) -> Option<Self> {
        self.amount_minor.checked_neg()
            .map(|amount_minor| Self::of_minor(self.currency, amount_minor))
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Money::of_minor(USD, 4_01),
    ///            Money::of_major(USD, 4).saturating_add(Money::of_minor(USD, 1)));
    /// assert_eq!(Money::max(USD),
    ///            Money::max(USD).saturating_add(Money::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn saturating_add(self, other: Self) -> Self {
        assert_eq!(self.currency, other.currency);
        Self::of_minor(self.currency, self.amount_minor.saturating_add(other.amount_minor))
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Money::of_minor(USD, 3_99),
    ///            Money::of_major(USD, 4).saturating_sub(Money::of_minor(USD, 1)));
    /// assert_eq!(Money::min(USD),
    ///            Money::min(USD).saturating_sub(Money::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    pub fn saturating_sub(self, other: Self) -> Self {
        assert_eq!(self.currency, other.currency);
        Self::of_minor(self.currency, self.amount_minor.saturating_sub(other.amount_minor))
    }

    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Money::of_major(USD, 8), Money::of_major(USD, 4).saturating_mul(2));
    /// assert_eq!(Money::max(USD), Money::max(USD).saturating_mul(2));
    /// assert_eq!(Money::max(USD), Money::min(USD).saturating_mul(-1));
    /// ```
    pub fn saturating_mul(self, n: i64) -> Self {
        Self::of_minor(self.currency, self.amount_minor.saturating_mul(n))
    }
}

impl fmt::Display for Money {
    /// Displays the value with `formatting::STYLE_GENERIC`. See the `formatting` module for other
    /// pre-defined and custom styles.
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!("1,234.56\u{a0}GBP", format!("{}", &Money::of_minor(GBP, 123456)));
    /// ```
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{}",
               formatting::format(formatting::generic_style(), self))
    }
}

impl FormattableMoney for Money {
    fn unformatted_minor_amount(&self) -> String {
        format!("{}", self.amount_minor)
    }

    fn currency(&self) -> Currency {
        self.currency
    }
}

impl ParseableMoney for Money {
    fn from_unformatted_minor_amount(currency: Currency, unformatted_minor_amount: &str)
                                     -> Result<Self, ::std::num::ParseIntError> {
        Ok(Self::of_minor(currency, try!(unformatted_minor_amount.parse::<i64>())))
    }
}

impl Add for Money {
    type Output = Money;
    /// Adds two `Money`s.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// let one = Money::of_major(USD, 1);
    /// assert_eq!(Money::of_major(USD, 2), one + one);
    /// // Likewise for refs:
    /// let one = Money::of_major(USD, 1);
    /// assert_eq!(Money::of_major(USD, 2), one + &one);
    /// assert_eq!(Money::of_major(USD, 2), &one + one);
    /// assert_eq!(Money::of_major(USD, 2), &one + &one);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_major(USD, 1) + Money::of_major(JPY, 1); // panics!
    /// ```
    ///
    /// Panics when addition of minor amounts would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, std::i64::MAX) + Money::of_minor(USD, 1); // panics!
    /// ```
    fn add(self, other: Money) -> Money {
        self.checked_add(other).expect("Money overflow")
    }
}

forward_ref_binop!{ impl Add, add for Money, Money }

impl Sub for Money {
    type Output = Money;
    /// Subtracts one `Money` from another.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// let one = Money::of_major(USD, 1);
    /// assert_eq!(Money::of_major(USD, 0), one - one);
    /// // Likewise for refs:
    /// assert_eq!(Money::of_major(USD, 0), one - &one);
    /// assert_eq!(Money::of_major(USD, 0), &one - one);
    /// assert_eq!(Money::of_major(USD, 0), &one - &one);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when currencies differ.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_major(USD, 1) - Money::of_major(JPY, 1); // panics!
    /// ```
    ///
    /// Panics when subtraction of minor amounts would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, std::i64::MIN) - Money::of_minor(USD, 1); // panics!
    /// ```
    fn sub(self, other: Money) -> Money {
        self.checked_sub(other).expect("Money sub would overflow")
    }
}

forward_ref_binop!{ impl Sub, sub for Money, Money }

impl Mul<i64> for Money {
    type Output = Money;
    /// Multiplies money by an integer.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// let two_usd = Money::of_major(USD, 2);
    /// let four_usd = Money::of_major(USD, 4);
    /// assert_eq!(four_usd, two_usd * 2);
    /// // Likewise for refs:
    /// assert_eq!(four_usd, two_usd * &2);
    /// assert_eq!(four_usd, &two_usd * 2);
    /// assert_eq!(four_usd, &two_usd * &2);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when multiplication of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, std::i64::MAX) * 2; // panics!
    /// ```
    fn mul(self, n: i64) -> Money {
        self.checked_mul(n).expect("Money mul would overflow.")
    }
}

forward_ref_binop!{ impl Mul, mul for Money, i64 }

impl Mul<f64> for Money {
    type Output = Money;
    /// Multiplies money by a float, rounding if needed.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// let one = Money::of_major(USD, 1);
    /// let one01 = Money::of_minor(USD, 1_01);
    /// assert_eq!(one01, one * 1.005001);
    /// assert_eq!(Money::of_minor(USD, 1_005_00), Money::of_major(USD, 1_000) * 1.005001);
    /// assert_eq!(Money::of_minor(USD, 10_050_01), Money::of_major(USD, 10_000) * 1.005001);
    /// // Likewise for refs:
    /// assert_eq!(one01, one * &1.005001);
    /// assert_eq!(one01, &one * 1.005001);
    /// assert_eq!(one01, &one * &1.005001);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when multiplication of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, std::i64::MAX) * 1.01; // panics!
    /// ```
    fn mul(self, n: f64) -> Money {
        self.checked_mul_f(n).expect("Money mul would overflow.")
    }
}

forward_ref_binop!{ impl Mul, mul for Money, f64 }

impl Div<i64> for Money {
    type Output = Money;
    /// Divides money by an integer.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// let two_usd = Money::of_major(USD, 2);
    /// let four01 = Money::of_minor(USD, 4_01);
    /// assert_eq!(two_usd, four01 / 2);
    /// // Likewise for refs:
    /// assert_eq!(two_usd, &four01 / 2);
    /// assert_eq!(two_usd, four01 / &2);
    /// assert_eq!(two_usd, &four01 / &2);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when division of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, std::i64::MIN) / -1; // panics!
    /// ```
    ///
    /// Panics when `n` is zero.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, 1) / 0; // panics!
    /// ```
    fn div(self, n: i64) -> Money {
        self.checked_div(n).expect(if n == 0 {
            "Money div by zero"
        } else {
            "Money div would overflow"
        })
    }
}

forward_ref_binop!{ impl Div, div for Money, i64 }

impl Rem<i64> for Money {
    type Output = Money;
    /// Remainder of dividing money by an integer.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// let one_cent = Money::of_minor(USD, 1);
    /// let four01 = Money::of_minor(USD, 4_01);
    /// assert_eq!(one_cent, four01 % 2);
    /// // Likewise for refs:
    /// assert_eq!(one_cent, &four01 % 2);
    /// assert_eq!(one_cent, four01 % &2);
    /// assert_eq!(one_cent, &four01 % &2);
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when division of minor amount would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, std::i64::MIN) % -1; // panics!
    /// ```
    ///
    /// Panics when `n` is zero.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// Money::of_minor(USD, 1) % 0; // panics!
    /// ```
    fn rem(self, n: i64) -> Money {
        self.checked_rem(n).expect(if n == 0 {
            "Money rem by zero"
        } else {
            "Money rem would overflow"
        })
    }
}

forward_ref_binop!{ impl Rem, rem for Money, i64 }

impl Neg for Money {
    type Output = Money;
    /// Negation.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Money::of_minor(USD, -1), -Money::of_minor(USD, 1));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when negation would overflow, which is only the case for the minimum value.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// -Money::min(USD); // panics!
    /// ```
    fn neg(self) -> Money {
        self.checked_neg().expect("Money neg would overflow")
    }
}

impl<'a> Neg for &'a Money {
    type Output = Money;
    /// Negation of ref.
    ///
    /// # Examples
    ///
    /// ```
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// assert_eq!(Money::of_minor(USD, -1), -(&Money::of_minor(USD, 1)));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics when negation would overflow.
    ///
    /// ```should_panic
    /// # use steel_cent::Money;
    /// # use steel_cent::currency::*;
    /// -(&Money::of_minor(USD, std::i64::MIN)); // panics!
    /// ```
    fn neg(self) -> Money {
        (*self).checked_neg().expect("Money neg would overflow")
    }
}

/// Compares to `Money` of the same currency only.
///
/// Returns `None` when currencies differ, causing all comparison operators to return false when
/// currencies differ.
impl PartialOrd for Money {
    fn partial_cmp(&self, other: &Money) -> Option<Ordering> {
        if self.currency == other.currency {
            self.amount_minor.partial_cmp(&other.amount_minor)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use currency::{EUR, JPY, USD};
    use super::Money;
    use std::cmp::Ordering;

    #[test]
    fn compares_equal() {
        let one_dollar = Money::of_minor(USD, 100);
        assert_eq!(one_dollar, Money::of_minor(USD, 100));
        assert!(one_dollar != Money::of_minor(USD, 101));
    }

    #[test]
    fn converts_major_to_minor() {
        assert_eq!(Money::of_minor(USD, 100), Money::of_major(USD, 1));
        assert_eq!(Money::of_minor(JPY, 100), Money::of_major(JPY, 100));
    }

    #[test]
    fn exposes_minor_part() {
        assert_eq!(34, Money::of_minor(USD, 1234).minor_part());
        assert_eq!(-34, Money::of_minor(USD, -1234).minor_part());
        assert_eq!(0, Money::of_minor(JPY, 1234).minor_part());
        assert_eq!(0, Money::of_minor(JPY, -1234).minor_part());
    }

    #[test]
    fn exposes_major_part() {
        assert_eq!(12, Money::of_minor(USD, 1234).major_part());
        assert_eq!(-12, Money::of_minor(USD, -1234).major_part());
        assert_eq!(1234, Money::of_minor(JPY, 1234).major_part());
        assert_eq!(-1234, Money::of_minor(JPY, -1234).major_part());
    }

    // -----------------
    // Custom Ops impls

    #[test]
    fn multiplies_by_int_or_float() {
        assert_eq!(Money::of_minor(USD, 400), &Money::of_minor(USD, 200) * 2);
        assert_eq!(Money::of_minor(USD, 180), &Money::of_minor(USD, 200) * 0.9);
    }

    #[test]
    fn multiplies_all_combinations_of_ref_and_consumed() {
        let two_usd = Money::of_major(USD, 2);
        assert_eq!(Money::of_major(USD, 4), two_usd.clone() * 2);
        assert_eq!(Money::of_major(USD, 1), two_usd.clone() * 0.5);
        assert_eq!(Money::of_major(USD, 4), two_usd.clone() * &2);
        assert_eq!(Money::of_major(USD, 1), two_usd.clone() * &0.5);
        assert_eq!(Money::of_major(USD, 4), &two_usd * 2);
        assert_eq!(Money::of_major(USD, 1), &two_usd * 0.5);
        assert_eq!(Money::of_major(USD, 4), &two_usd * &2);
        assert_eq!(Money::of_major(USD, 1), &two_usd * &0.5);
    }

    #[test]
    fn divide_and_remainder_by_int() {
        let ten_usd = Money::of_major(USD, 10);
        let three_thirty_three_usd = Money::of_minor(USD, 333);
        let one_cent = Money::of_minor(USD, 1);
        assert_eq!(three_thirty_three_usd, ten_usd.clone() / 3);
        assert_eq!(three_thirty_three_usd, ten_usd.clone() / &3);
        assert_eq!(three_thirty_three_usd, &ten_usd / 3);
        assert_eq!(three_thirty_three_usd, &ten_usd / &3);
        assert_eq!(one_cent, ten_usd.clone() % 3);
        assert_eq!(one_cent, ten_usd.clone() % &3);
        assert_eq!(one_cent, &ten_usd % 3);
        assert_eq!(one_cent, &ten_usd % &3);
    }

    #[test]
    fn unary_negation() {
        let one_dollar = Money::of_major(USD, 1);
        assert_eq!(Money::of_major(USD, -1), -&one_dollar);
        assert_eq!(Money::of_major(USD, -1), -one_dollar);
    }

    // ----------
    // cmp

    #[test]
    fn compares_when_currencies_match() {
        let one_dollar = Money::of_major(USD, 1);
        let two_dollars = Money::of_major(USD, 2);
        assert_eq!(Some(Ordering::Less), one_dollar.partial_cmp(&two_dollars));
        assert_eq!(Some(Ordering::Equal), one_dollar.partial_cmp(&one_dollar));
        assert_eq!(Some(Ordering::Greater),
                   two_dollars.partial_cmp(&one_dollar));
        assert!(one_dollar < two_dollars);
        assert!(one_dollar <= two_dollars);
        assert!(one_dollar <= one_dollar);
        assert!(two_dollars > one_dollar);
        assert!(two_dollars >= two_dollars);
        assert!(two_dollars >= two_dollars);
    }

    #[test]
    fn does_not_compare_when_currencies_differ() {
        let one_dollar = Money::of_major(USD, 1);
        let one_euro = Money::of_major(EUR, 1);
        assert_eq!(None, one_dollar.partial_cmp(&one_euro));
        assert_eq!(None, one_euro.partial_cmp(&one_dollar));
        assert!(!(one_dollar < one_euro));
        assert!(!(one_euro < one_dollar));
        assert!(!(one_dollar <= one_euro));
        assert!(!(one_euro <= one_dollar));
        assert!(!(one_dollar > one_euro));
        assert!(!(one_euro > one_dollar));
        assert!(!(one_dollar >= one_euro));
        assert!(!(one_euro >= one_dollar));
    }

    // --------
    // ParseableMoney

    #[test]
    fn creates_from_str() {
        use std::error::Error;
        assert_eq!(Ok(Money::of_minor(USD, 1234_56)),
                   ::formatting::ParseableMoney::from_unformatted_minor_amount(USD, "123456"));
        assert_eq!(Ok(Money::of_minor(USD, -1234_56)),
                   ::formatting::ParseableMoney::from_unformatted_minor_amount(USD, "-123456"));
        let parse_failure = ::formatting::ParseableMoney::from_unformatted_minor_amount(USD, "abc");
        assert_eq!("abc".parse::<i64>().map(|n| Money::of_minor(USD, n)), parse_failure);
        assert_eq!("invalid digit found in string", parse_failure.unwrap_err().description())
    }
}
